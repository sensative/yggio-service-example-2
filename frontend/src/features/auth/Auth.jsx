import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useSearchParams, useNavigate } from 'react-router-dom';
import { submitAuthTokenToBackendAsync, selectUser } from './authSlice';
import { ROUTES } from '../../common/constants';
import CenteredLoadingSpinner from '../../common/components/CenteredLoadingSpinner';

const Auth = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();
  const user = useSelector(selectUser);

  useEffect(() => {
    const token = searchParams.get('code');
    if (token) {
      dispatch(submitAuthTokenToBackendAsync(token));
    }
  }, [searchParams]);

  useEffect(() => {
    if (user) {
      navigate(ROUTES.devicesRoute);
    }
  }, [user]);

  return <CenteredLoadingSpinner />;
};

export default Auth;
