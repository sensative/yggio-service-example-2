import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Container, Paper, styled } from '@mui/material';
import { selectAuthInfo, fetchAuthInfoAsync } from './authSlice';
import { oauthURL } from '../../common/constants';
import { StyledPrimaryButton } from '../../common/styling/buttons';

const StyledContainer = styled(Container)`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const StyledBox = styled(Paper)`
  width: 300px;
  height: 200px;
  margin-top: 25%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const Login = () => {
  const dispatch = useDispatch();
  const authInfo = useSelector(selectAuthInfo);

  const handleLogin = () => {
    dispatch(fetchAuthInfoAsync());
  };

  const redirectUri = authInfo?.redirectUris?.browser;
  if (redirectUri) {
    window.location = `${oauthURL}?client_id=${authInfo.clientId}&response_type=code&redirect_uri=${redirectUri}`;
  }

  return (
    <StyledContainer>
      <StyledBox elevation={2}>
        <h2>Please login to continue</h2>
        <StyledPrimaryButton variant="contained" size="small" onClick={handleLogin}>
          Login
        </StyledPrimaryButton>
      </StyledBox>
    </StyledContainer>
  );
};

export default Login;
