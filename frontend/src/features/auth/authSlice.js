import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import authService from './authAPI';

const initialState = {
  status: 'idle',
  authInfo: null,
  user: null,
};

export const fetchAuthInfoAsync = createAsyncThunk('auth/fetchAuthInfo', async () => {
  const authInfo = await authService.getAuthInfo();
  return authInfo;
});

export const submitAuthTokenToBackendAsync = createAsyncThunk('auth/submitAuthTokenToBackend', async (token) => {
  const user = await authService.submitAuthTokenToBackend(token);
  return user;
});

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  extraReducers: (builder) => {
    builder
      .addCase(fetchAuthInfoAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchAuthInfoAsync.fulfilled, (state, action) => {
        state.status = 'idle';
        state.authInfo = action.payload;
      })
      .addCase(submitAuthTokenToBackendAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(submitAuthTokenToBackendAsync.fulfilled, (state, action) => {
        state.status = 'idle';
        state.user = action.payload;
      });
  },
});

export const selectAuthInfo = (state) => state.auth.authInfo;
export const selectUser = (state) => state.auth.user;

export default authSlice.reducer;
