import axios from 'axios';
import { backendHost } from '../../common/constants';

const getAuthInfo = () => axios.get(`${backendHost}/api/auth/info`).then((response) => response.data);

const submitAuthTokenToBackend = async (token) => {
  const url = `${backendHost}/api/auth/code?code=${token}&redirect_uri=browser`;
  const user = await axios.get(url).then((response) => response.data);
  return user;
};

const authService = {
  getAuthInfo,
  submitAuthTokenToBackend,
};

export default authService;
