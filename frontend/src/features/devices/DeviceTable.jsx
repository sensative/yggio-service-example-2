import * as React from 'react';
import { styled, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from '@mui/material';
import DeviceTableRow from './DeviceTableRow';

const StyledTableContainer = styled(TableContainer)`
  margin-bottom: 2rem;
`;

const StyledTable = styled(Table)`
  min-width: 650;
`;

const StyledTableHead = styled(TableHead)`
  background-color: #e0eed7;
`;

const StyledTableCell = styled(TableCell)`
  font-weight: 600;
`;

const DeviceTable = ({ devices, onDeleteDevice, onSubscribe }) => (
  <StyledTableContainer component={Paper} elevation={2}>
    <StyledTable aria-label="simple table">
      <StyledTableHead>
        <TableRow>
          <StyledTableCell>Device</StyledTableCell>
          <StyledTableCell>Description</StyledTableCell>
          <StyledTableCell>Value</StyledTableCell>
          <StyledTableCell>Updated</StyledTableCell>
          <StyledTableCell> </StyledTableCell>
          <StyledTableCell> </StyledTableCell>
        </TableRow>
      </StyledTableHead>
      <TableBody>
        {devices.map((device) => (
          <DeviceTableRow key={device._id} device={device} onDeleteDevice={onDeleteDevice} onSubscribe={onSubscribe} />
        ))}
      </TableBody>
    </StyledTable>
  </StyledTableContainer>
);

export default DeviceTable;
