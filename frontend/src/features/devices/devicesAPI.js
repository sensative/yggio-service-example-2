import axios from 'axios';
import { backendHost } from '../../common/constants';

const getDevices = async () => axios.get(`${backendHost}/api/devices`).then((response) => response);

const postDevice = async (device) => axios.post(`${backendHost}/api/devices`, device).then((response) => response);

const getDeviceSubscriptions = async (deviceId) =>
  axios.get(`${backendHost}/api/subscriptions`, { params: { iotnode: deviceId } }).then((response) => response.data);

const postDeviceSubscription = async (deviceId) => {
  const payload = {
    name: `device${deviceId}-channel`,
    nodeId: deviceId,
    protocol: 'http',
  };
  axios.post(`${backendHost}/api/subscriptions`, payload).then((response) => response.data);
};

const deleteDevice = async (deviceId) => axios.delete(`${backendHost}/api/devices/${deviceId}`);

const devicesService = {
  getDevices,
  postDevice,
  getDeviceSubscriptions,
  postDeviceSubscription,
  deleteDevice,
};

export default devicesService;
