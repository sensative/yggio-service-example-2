import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { styled } from '@mui/material';
import {
  fetchDevicesAsync,
  selectDevices,
  deleteDeviceAsync,
  deleteAllDevicesAsync,
  postDummyDevicesAsync,
  postDeviceSubscriptionAsync,
  selectIsLoading,
} from './devicesSlice';
import DeviceTable from './DeviceTable';
import CenteredLoadingSpinner from '../../common/components/CenteredLoadingSpinner';
import { StyledPrimaryButton, StyledSecondaryButton } from '../../common/styling/buttons';

const StyledAddMultipleDevicesButton = styled(StyledPrimaryButton)`
  margin-bottom: 1rem;
  margin-right: 4px;
`;

const StyledDeleteMultipleDevicesButton = styled(StyledSecondaryButton)`
  margin-bottom: 1rem;
`;

const DevicesPage = () => {
  const dispatch = useDispatch();
  const devices = useSelector(selectDevices);
  const isLoading = useSelector(selectIsLoading);

  useEffect(() => {
    dispatch(fetchDevicesAsync());
  }, []);

  const handlePostMultipleDummyDevices = () => {
    dispatch(postDummyDevicesAsync(10));
  };

  const handleDeleteAllDevices = () => {
    dispatch(deleteAllDevicesAsync());
  };

  const handleDeleteDevice = (id) => {
    dispatch(deleteDeviceAsync(id));
  };

  const handleSubscribe = (id) => {
    dispatch(postDeviceSubscriptionAsync(id));
  };

  return (
    <div>
      <h2>My devices</h2>
      {isLoading && <CenteredLoadingSpinner />}
      <div>
        <StyledAddMultipleDevicesButton variant="contained" size="small" onClick={handlePostMultipleDummyDevices}>
          Add dummy devices
        </StyledAddMultipleDevicesButton>
        <StyledDeleteMultipleDevicesButton
          disabled={devices && !devices.length}
          variant="contained"
          size="small"
          onClick={handleDeleteAllDevices}
        >
          Delete all
        </StyledDeleteMultipleDevicesButton>
      </div>
      <DeviceTable devices={devices} onDeleteDevice={handleDeleteDevice} onSubscribe={handleSubscribe} />
    </div>
  );
};

export default DevicesPage;
