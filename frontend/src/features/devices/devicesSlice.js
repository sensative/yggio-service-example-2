/* eslint-disable default-param-last */
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import devicesService from './devicesAPI';

const initialState = {
  status: 'idle',
  devices: [],
};

export const fetchDeviceSubscriptionsAsync = createAsyncThunk('devices/fetchDeviceSubscriptions', async (deviceId) => {
  const deviceSubscriptions = await devicesService.getDeviceSubscriptions(deviceId);
  return { deviceSubscriptions, deviceId };
});

// eslint-disable-next-line no-unused-vars
export const fetchDevicesAsync = createAsyncThunk('devices/fetchDevices', async (arg = {}, thunkAPI) => {
  const response = await devicesService.getDevices();
  const devices = response.data;
  if (response.status === 200) {
    devices.forEach((device) => {
      thunkAPI.dispatch(fetchDeviceSubscriptionsAsync(device._id));
    });
  } else {
    throw new Error(devices);
  }
  return devices;
});

export const postDeviceAsync = createAsyncThunk('devices/postDevice', async (device, thunkAPI) => {
  const response = await devicesService.postDevice(device);
  if (response.status === 201) {
    thunkAPI.dispatch(fetchDeviceSubscriptionsAsync(response?.data?._id));
  } else {
    throw new Error(response);
  }
  return response.data;
});

export const postDummyDevicesAsync = createAsyncThunk('devices/postDummyDevices', async (amount = 10, thunkAPI) => {
  for (let index = 0; index < amount; index += 1) {
    const newName = `enum${Math.floor(Math.random() * 10000)}`;
    const payload = {
      latlng: [],
      name: newName,
      description: 'this device is for testing purpose',
      allowedValues: ['state1', 'state2'],
      value: 'state2',
      nodeType: 'enum-state',
    };
    thunkAPI.dispatch(postDeviceAsync(payload));
  }
  return 'added';
});

export const postDeviceSubscriptionAsync = createAsyncThunk('devices/postDeviceSubscription', async (deviceId) => {
  await devicesService.postDeviceSubscription(deviceId);
  return deviceId;
});

export const deleteDeviceAsync = createAsyncThunk('devices/deleteDevice', async (deviceId) => {
  await devicesService.deleteDevice(deviceId);
  return deviceId;
});

// eslint-disable-next-line no-unused-vars
export const deleteAllDevicesAsync = createAsyncThunk('devices/deleteAllDevices', async (arg = {}, thunkAPI) => {
  const { devices } = thunkAPI.getState().devices;
  devices.forEach((d) => {
    thunkAPI.dispatch(deleteDeviceAsync(d._id));
  });
  return 'deleted';
});

export const devicesSlice = createSlice({
  name: 'devices',
  initialState,
  extraReducers: (builder) => {
    builder
      .addCase(fetchDevicesAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchDevicesAsync.fulfilled, (state, action) => {
        state.status = 'idle';
        state.devices = action.payload;
      })
      .addCase(fetchDevicesAsync.rejected, (state) => {
        state.status = 'idle';
      })
      .addCase(postDeviceAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(postDeviceAsync.fulfilled, (state, action) => {
        state.status = 'idle';
        state.devices.push(action.payload);
      })
      .addCase(postDeviceAsync.rejected, (state) => {
        state.status = 'idle';
      })
      .addCase(fetchDeviceSubscriptionsAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchDeviceSubscriptionsAsync.fulfilled, (state, action) => {
        state.status = 'idle';
        const fetchedSubscriptions = action.payload.deviceSubscriptions;
        const device = state.devices.find((device) => device._id === action.payload.deviceId);
        if (fetchedSubscriptions.length) {
          device.subscribed = true;
        } else {
          device.subscribed = false;
        }
      })
      .addCase(postDeviceSubscriptionAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(postDeviceSubscriptionAsync.fulfilled, (state, action) => {
        state.status = 'idle';
        const deviceId = action.payload;
        const device = state.devices.find((device) => device._id === deviceId);
        device.subscribed = true;
      })
      .addCase(deleteDeviceAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(deleteDeviceAsync.fulfilled, (state, action) => {
        state.status = 'idle';
        const deviceId = action.payload;
        state.devices = state.devices.filter((device) => device._id !== deviceId);
      })
      .addCase(deleteAllDevicesAsync.pending, (state) => {
        state.status = 'loading';
      });
  },
});

export const selectDevices = (state) => state.devices.devices;
export const selectIsLoading = (state) => state.devices.status === 'loading';

export default devicesSlice.reducer;
