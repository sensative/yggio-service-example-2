import React from 'react';
import { TableCell, TableRow } from '@mui/material';
import { StyledPrimaryButton, StyledSecondaryButton } from '../../common/styling/buttons';

const DeviceTableRow = ({ device, onDeleteDevice, onSubscribe }) => {
  const formatDate = (date) => {
    const isoDate = new Date(date);
    return `${isoDate.toLocaleDateString()} - ${isoDate.toLocaleTimeString('en-US')}`;
  };

  return (
    <TableRow>
      <TableCell component="th" scope="row">
        {device.name}
      </TableCell>
      <TableCell>{device.description || 'no data'}</TableCell>
      <TableCell>{device.value || 'no data'}</TableCell>
      <TableCell>{formatDate(device.updatedAt)}</TableCell>
      <TableCell>
        {Object.prototype.hasOwnProperty.call(device, 'subscribed') && (
          <StyledPrimaryButton
            size="small"
            variant="contained"
            disabled={device.subscribed}
            onClick={() => onSubscribe(device._id)}
          >
            {device.subscribed ? 'Subscribed' : 'Subscribe'}
          </StyledPrimaryButton>
        )}
      </TableCell>
      <TableCell>
        <StyledSecondaryButton size="small" variant="contained" onClick={() => onDeleteDevice(device._id)}>
          Delete
        </StyledSecondaryButton>
      </TableCell>
    </TableRow>
  );
};

export default DeviceTableRow;
