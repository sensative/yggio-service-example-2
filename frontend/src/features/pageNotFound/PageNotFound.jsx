import React from 'react';

const PageNotFound = () => (
  <div>
    <p>There is nothing here!</p>
  </div>
);

export default PageNotFound;
