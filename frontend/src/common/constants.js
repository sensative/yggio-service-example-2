// eslint-disable-next-line import/prefer-default-export
export const ROUTES = {
  loginRoute: '/',
  authRoute: '/oauth',
  devicesRoute: '/devices',
};

export const portalHost = 'https://yggio-beta.sensative.net';
export const backendHost = 'http://localhost:9999';
export const oauthURL = `${portalHost}/oauth`;
