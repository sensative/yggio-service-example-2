import React from 'react';
import { styled, Backdrop, Box, CircularProgress } from '@mui/material';

const StyledBackdropDiv = styled('div')`
  opacity: 0.2;
`;

const StyledBackdrop = styled(Backdrop)`
  z-index: ${(props) => props.theme.zIndex.drawer + 1};
`;

const StyledSpinnerBox = styled(Box)`
  display: flex;
  justify-content: center;
  padding: 2;
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
`;

const StyledCircularProgress = styled(CircularProgress)`
  color: #397941;
`;

const CenteredLoadingSpinner = () => (
  <>
    <StyledBackdropDiv>
      <StyledBackdrop open />
    </StyledBackdropDiv>
    <StyledSpinnerBox>
      <StyledCircularProgress />
    </StyledSpinnerBox>
  </>
);

export default CenteredLoadingSpinner;
