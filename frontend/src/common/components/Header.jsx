import React from 'react';
import { AppBar, Box, Toolbar, Typography, styled } from '@mui/material';
import logo from '../../assets/img/yggio-logo.png';

const StyledBox = styled(Box)`
  flex-grow: 1;
`;

const StyledAppBar = styled(AppBar)`
  color: #397941;
`;

const StyledImg = styled('img')`
  height: 60px;
`;

const StyledTypography = styled(Typography)`
  font-family: 'Julius Sans One', sans-serif;
  font-weight: 700;
  flex-grow: 1;
  margin-left: 1rem;
`;

const Header = () => (
  <StyledBox>
    <StyledAppBar position="static" color="transparent">
      <Toolbar>
        <StyledImg src={logo} alt="Logo" />
        <StyledTypography variant="h6" component="div">
          Yggio Service Demo
        </StyledTypography>
      </Toolbar>
    </StyledAppBar>
  </StyledBox>
);

export default Header;
