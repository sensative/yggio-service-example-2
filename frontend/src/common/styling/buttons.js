import { styled, Button } from '@mui/material';

export const StyledPrimaryButton = styled(Button)`
  background-color: #228f30;
  &:hover {
    background-color: #397941;
  }
`;

export const StyledSecondaryButton = styled(Button)`
  background-color: grey;
  color: #fff;
  &:hover {
    background-color: #5e5e5e;
  }
`;
