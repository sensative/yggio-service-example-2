import React from 'react';
import { Routes, Route } from 'react-router-dom';
import { ROUTES } from './common/constants';

// VIEWS
import Layout from './Layout';
import PageNotFoundComponent from './features/pageNotFound/PageNotFound';
import LoginPage from './features/auth/Login';
import OauthPage from './features/auth/Auth';
import DevicesPage from './features/devices/DevicesPage';

const { loginRoute, authRoute, devicesRoute } = ROUTES;

const Routing = () => (
  <Routes>
    <Route path={loginRoute} element={<Layout />}>
      <Route index element={<LoginPage />} />
      <Route path={authRoute} element={<OauthPage />} />
      <Route path={devicesRoute} element={<DevicesPage />} />

      <Route path="*" element={<PageNotFoundComponent />} />
    </Route>
  </Routes>
);

export default Routing;
