import { configureStore } from '@reduxjs/toolkit';
import authReducer from '../features/auth/authSlice';
import devicesReducer from '../features/devices/devicesSlice';

const store = configureStore({
  reducer: {
    auth: authReducer,
    devices: devicesReducer,
  },
});

export default store;
