import React from 'react';
import { Outlet } from 'react-router-dom';
import { Container } from '@mui/material';
import Header from './common/components/Header';

const Layout = () => (
  <div>
    <Header />
    <Container maxWidth="md">
      <Outlet />
    </Container>
  </div>
);

export default Layout;
