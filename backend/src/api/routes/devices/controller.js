'use strict';

const {
  routes: { getNodes, createNode, deleteNode },
} = require('yggio-connect');

const fetch = (req, res, next) => {
  const { user } = req.session;
  getNodes(user).then(res.status(200).json.bind(res)).catch(next);
};

const createDevice = (req, res, next) => {
  const { user } = req.session;
  const node = req.body;
  createNode(user, node).then(res.status(201).json.bind(res)).catch(next);
};

const deleteDevice = (req, res, next) => {
  const { user } = req.session;
  const id = req.params.id;
  deleteNode(user, id).then(res.status(200).json.bind(res)).catch(next);
};

module.exports = {
  fetch,
  createDevice,
  deleteDevice,
};
