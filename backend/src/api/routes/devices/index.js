'use strict';

const router = require('express').Router();
const controller = require('./controller');

router.get('/', controller.fetch);
router.post('/', controller.createDevice);
router.delete('/:id', controller.deleteDevice);

module.exports = router;
